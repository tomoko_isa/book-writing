## ●原稿の表記統一について
・本文の語尾はですます調にしてください。
（ただ、もしご希望で、で・ある調が良ければそれでそろえることも可能です）

・節のタイトルだけ「で・ある調」でも構いません（ただ、全体でそろえてください）。もちろん節のタイトルも「ですます調」でも良いのですが、長くなりすぎる傾向があるので･･･。

・ソフト名、サービス名、サイト名は、基本的に公式の表記に合わせてください。手打ちせず、サイトからコピペしたりしてください（打ち間違いが多いので）。

・カッコに入れて何かを補足する場合、カッコの後ろに。をつけてください。
例）そういった事例が多くあります（あくまで筆者の見聞きした範囲ですが）。


## ●用字用語の統一
・ユーザー、サーバー、ヘッダー、フッター、ウィンドウ、ソフトウェア
など。
音引き（「ユーザー」の最後の「−」などを音引きといいます）は、ありかなしか、どちらかに揃っていれば大丈夫です。

・数字は「1つ」「2つ」と表記（数字は半角）。

・ただし、サイトやソフトウェアの操作解説部分は、その表記に合わせてください。


## ●カッコ類について
・サイト名、サービス名などは基本的に「」でくくってください。
例）「Facebook」「Twitter」

・ソフトのメニューなども「」でくくってください。
例）ブラウザーの「表示」メニューから…

・メニューが長い場合は間を「→」で区切ってください。
例）メニューから「表示→ツールバー→標準」を選択します。


## ●図版
・基本的にはWindows版、Macintosh版、どちらかで統一してください。

・お使いのモニターが大きい場合、ブラウザをフル画面にしてキャプチャを撮ると、横幅が大きすぎる場合があります。ブラウザの横の解像度を1000ピクセル前後にしてキャプチャしてください。

・基本的にはブラウザ全体をキャプチャしてください（説明に関係あるところのみではなくて、全体の方が扱いやすいのです）。

・画像はWordなどに貼り付けず、PNGやPDF、JPGなどの形式で保存してください。

・図について、「ここに入力する」というような意味での囲みを付けたい場合があると思います。その場合、囲みをつけない元ファイルを残したまま、別ファイルとして、囲みをつけたファイルをご用意ください。



## ●図への参照
・本文中に、［fig1］などのような表記（この場合の図のファイル名はfig1.png）で、適宜図版への参照をいれてください。


## ●ひらく言葉、ひらかない言葉

※「ひらく」はひらがなにする、という意味です。


+ ○〜すること 　　×〜する事
+ ○〜するごと 　　×〜する毎
+ ○〜するたび 　　×〜する度
+ ○〜するため 　　×〜する為
+ ○〜するほど 　　×〜する程
+ ○〜につき　 　　×〜に付き
+ ○〜のとおり　 　×〜の通り
+ ○〜のころ　 　　×〜の頃
+ ○〜のほか　 　　×〜の他
+ ○〜するとき　 　×〜する時
+ ○〜ところ　　 　×〜所　（現状のところ、など）
+ ○〜のほうが 　　×〜の方が
+ ○〜するわけ 　　×〜する訳
+ ○ありうる 　　　×〜あり得る
+ ○〜していく 　　×〜して行く
+ ○いただく　 　　×頂く
+ ○できる　 　　　×出来る
+ ○わかる　 　　　×分かる
+ ○〜のごとく　 　×〜の如く
+ ○〜のような　 　×〜の様な
+ ○さまざま　 　　×様々
+ ○しやすい　 　　×し易い
+ ○しづらい　 　　×し辛い
+ ○やさしい　 　　×易しい
+ ○よい　 　　　　×良い（場合によってはOK）
+ ○あらかじめ　 　×予め
+ ○いたします　 　×致します
+ ○および　 　　　×及び
+ ○さらに　 　　　×更に
+ ○すでに　 　　　×既に
+ ○すなわち　 　　×即ち
+ ○あらかじめ　 　　×予め
+ ○すべて　 　　　×全て
+ ○ぜひ　 　　　　×是非
+ ○そば　 　　　　×側
+ ○たくさん　　　 ×沢山
+ ○ただ　　　　　 ×只
+ ○ただし　　　　 ×但し
+ ○など　　　　　 ×等
+ ○ならびに　　　 ×並びに
+ ○まず　　　　　 ×先ず
+ ○また　　　　　 ×又
+ ○まで　　　　　 ×迄
+ ○なお　　　　　 ×尚
+ ○まったく　　　 ×全く
+ ○ください　　　 ×ください


+ ○先ほど　　　　 ×さきほど
+ ○後ほど　　　　 ×のちほど
+ ○特に　　　　　 ×とくに
+ ○詳しく　　　　 ×くわしく
+ ○〜する上　　　 ×〜するうえ
+ ○最も　　　　　 ×もっとも






